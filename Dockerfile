# syntax=docker/dockerfile:1.4
ARG BASE_IMAGE=registry.gitlab.com/ds-unisg/servers/python-docker/py39/cpu:cli
FROM $BASE_IMAGE AS base

FROM base as poetry
USER "${USER_NAME}"
COPY "./poetry.lock" "./pyproject.toml" "${USER_HOME}"
RUN \
  --mount=type=cache,uid="${USER_ID}",target="${USER_HOME}/.cache/pypoetry/artifacts" \
  --mount=type=cache,uid="${USER_ID}",target="${USER_HOME}/.cache/pypoetry/cache" \
  <<EOF

  if test -f "/NGC-DL-CONTAINER-LICENSE"; then
    sed -i -E '/^torch/s/source = "torch-cpu/source = "torch-gpu"/' pyproject.toml
    poetry lock --no-update
  fi

  poetry install

EOF

FROM poetry AS final
USER root
WORKDIR "${USER_HOME}/app"
COPY . "${USER_HOME}/app/"
CMD ["gunicorn", "--workers=1", "--bind=0.0.0.0:8000", "--name=artist", "--timeout=360", "--capture-output", "server:app"]

