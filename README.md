# ARTIST - ARgumentative wriTIng SupporT

<div align="center">
	<img src="static/artist8.png" height=200px />
</div>


## Motivation
The study investigates a new generation of digital support for academic writing and offers deep insights into how AI technology can change academic writing and impacts changes in instructional approaches and adaptive coaching.


<div align="center">
	<img src="static/artist.png" width="100%" />
</div>


The framework provides **visual feedback on the quality of student-written argumentative essays**. By mining *argumentative components* and their *relationships* and assessing a set of different *quality dimensions* of the written text, it provides continuous and adaptive feedback to the user.

Type of Feedback:
* **In-text highlighting** of argumentative components (major claims, claims and premises)
* **Graph-based visualization** of argumentative relationships (support)
* **Chart-based visualization** of quality dimensions (readability, coherence and persuasiveness)

For more information, see [https://ai4writing.net/](https://ai4writing.net/).


## Online Demo
[Artist - Feedback tool](https://artist.datascience-nlp.ai/)

## Installation
We suggest to use a virtual environment or docker.
Successfully tested with Python 3.9.

_Change the endpoint to `localhost:5130` in the HTML pages, otherwise it will use the demo API_

(1) Clone:

    https://gitlab.com/ds-unisg/aied2024

(2) Build docker:

    docker build -t artist .
    docker-compose up --build

(3) Start docker:

    docker run --name=artist -p 5130:5130 artist

(4) Usage: Open the [index.html](./index.html) file in your browser.



## Contributors (alphabetical order)
* Christina Niklaus
* Jannis Katis
* Rositsa Ivanova
* Reto Gubelmann
* Siegfried Handschuh


<div align="center">
	<img src="static/ICS-HSG_Logo_EN.png" height=80px />
</div>
    
