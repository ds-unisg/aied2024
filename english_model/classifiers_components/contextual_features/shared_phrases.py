### shared nouns or verbs with the introduction or conclusion phrase


def shared_noun_phrases(s, nouns):
    count = 0
    for nc in s.noun_chunks:
        if nc in nouns:
            count += 1
    return count


def shared_verb_phrases(s, verb_phrases):
    count = 0
    for v in verb_phrases:
        if str(v) in s.text:
            count += 1
    return count
