import pickle
import sys
from pathlib import Path

import pandas as pd
import spacy
from spacy.matcher import Matcher

# appending a path
sys.path.append("english_model/classifiers_components")

from contextual_features import shared_phrases, type_indicators
from indicators import argumentative_discourse_markers, reference_to_first_person
from structural_features import sentence_closes_with_question_mark, sentence_length
from syntactic_features import (
    contains_adverb,
    contains_modal_verb,
    contains_verb,
    dependency,
    ner,
    number_of_subclauses,
    parse_tree_depth,
    tense_of_main_verb,
)

nlp = spacy.load("en_core_web_sm")


def get_label(sentence, annotations):
    for ann in annotations:
        if ann[0] in sentence:
            assigned_label = ann[1]
            return assigned_label

    return "None"


"""The following 2 functions are relevant for the contextual features"""


### This function returns all the noun chunks of a sentence - used for the introduction and conclusion
def noun_chunks(sen):
    nouns = []
    for noun in sen.noun_chunks:
        nouns.append(noun)

    return nouns


### This function returns the verb phrases of a sentence - used for the introduction and conclusion
def verb_phrases(sen):
    ### found those patterns at https://stackoverflow.com/questions/47856247/extract-verb-phrases-using-spacy
    ### maybe we can use more/less
    pattern = [
        {"POS": "VERB", "OP": "?"},
        {"POS": "ADV", "OP": "*"},
        {"POS": "AUX", "OP": "*"},
        {"POS": "VERB", "OP": "+"},
    ]

    ### instantiate a Matcher instance
    matcher = Matcher(nlp.vocab)
    matcher.add("Verb phrase", [pattern])

    ### call the matcher to find matches
    verbPhrases = [sen[start:end] for _, start, end in matcher(sen)]

    return verbPhrases


def create_data_set(text, cv):
    f = []

    doc = nlp(text)

    sentences = [sent for sent in doc.sents]

    ### calculating the average tokens of a sentence - relative for Structural Features
    total = 0

    for p in range(len(sentences)):
        total += len(sentences[p])

    avg_sent_length = total / len(sentences)

    ### to be used in some contextual features (from shared_phrases) - can also be moved away from the dataframe
    ### into the features but that would mean sending unnecessarily sending a lot of sentences
    ### relative for Contextual Features
    intro_nouns = noun_chunks(sentences[0])
    conc_nouns = noun_chunks(sentences[len(sentences) - 1])
    intro_verbs = verb_phrases(sentences[0])
    conc_verbs = verb_phrases(sentences[len(sentences) - 1])

    ### extracting the number of subclauses from the whole document - relative for Syntactic Features
    total_subclauses = number_of_subclauses.num_of_subclauses(doc)

    ### max dependency tree depth for the whole document - relative for Syntactic Features
    doc_parse_tree_depth = parse_tree_depth.parse_tree_depth(doc)

    for j in range(len(sentences)):
        features = {}

        if sentences[j] is not None:
            sen = str(sentences[j])

            features["text"] = sen

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ STRUCTURAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            if j == 0:
                features["is_first"] = True
            else:
                features["is_first"] = False

            if j == len(sentences) - 1:
                features["is_last"] = True
            else:
                features["is_last"] = False

            features[
                "closing_question_mark"
            ] = sentence_closes_with_question_mark.sentence_closes_with_question_marks(
                sentences[j]
            )  # bool

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ INDICATOR FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            features[
                "causal_markers"
            ] = argumentative_discourse_markers.contains_causal_markers(
                sentences[j]
            )  # bool
            features[
                "conditional_markers"
            ] = argumentative_discourse_markers.contains_conditional_markers(
                sentences[j]
            )  # bool
            features[
                "adversative_markers"
            ] = argumentative_discourse_markers.contains_adversative_markers(
                sentences[j]
            )  # bool
            features[
                "consecutive_markers"
            ] = argumentative_discourse_markers.contains_consecutive_markers(
                sentences[j]
            )  # bool
            features[
                "concessive_markers"
            ] = argumentative_discourse_markers.contains_concessive_markers(
                sentences[j]
            )  # bool
            features["first_person"] = reference_to_first_person.contains_first_person(
                sentences[j]
            )  # bool

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ CONTEXTUAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            features["thesis_indicator"] = type_indicators.thesis_context(sen)  # bool
            features["rebuttal_indicator"] = type_indicators.rebuttal_context(
                sen
            )  # bool
            features["backward_context"] = type_indicators.backward_context(sen)
            features["forward_context"] = type_indicators.forward_context(sen)

            """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ SYNTACTIC AND OTHER FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

            ### newly added
            features["parse_tree_depth"] = doc_parse_tree_depth  # numeric
            features["sentence_dependency_depth"] = dependency.tree_depth(sentences[j])
            # features["tense"] = tense_of_main_verb.get_tense(
            #   sentences[j]
            # )
            features["number_of_subclauses"] = total_subclauses  # numeric

            f.append(features)

    df = pd.DataFrame.from_dict(f)

    X1 = cv.transform(df.text)
    df = df.drop(columns="text")
    count_vect_df = pd.DataFrame(X1.todense(), columns=cv.get_feature_names_out())

    combined_df = pd.concat([df, count_vect_df], axis=1)

    return combined_df


def predict_AAE(text):
    ### load the model from disk
    loaded_model = pickle.load(
        open(
            Path(__file__)
            .parent.parent.parent.joinpath("models")
            .joinpath("random_forest")
            .joinpath("trained_on_AAE")
            .joinpath("random_forest.pkl"),
            "rb",
        )
    )
    loaded_count_vectorizer = pickle.load(
        open(
            Path(__file__)
            .parent.parent.parent.joinpath("models")
            .joinpath("random_forest")
            .joinpath("trained_on_AAE")
            .joinpath("count_vectorizer_random_forest.pkl"),
            "rb",
        )
    )

    X_unseen = create_data_set(text, loaded_count_vectorizer)

    pred = loaded_model.predict(X_unseen)
    return pred

