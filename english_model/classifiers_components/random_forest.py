import pickle
from pathlib import Path

import dataframe

# import pandas as pd
import spacy
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report

nlp = spacy.load("en_core_web_sm")


cv, X_train, X_test, y_train, y_test = dataframe.create_train_test_split()


classifier = RandomForestClassifier()
classifier.fit(X_train, y_train.values.ravel())


y_pred = classifier.predict(X_test)
print(y_pred)

# Accuracy
accuracy = accuracy_score(y_test, y_pred)
print("random forest: ", accuracy)


### Classification Report
target_names = ["MajorClaim", "Claim", "Premise", "None"]
print(classification_report(y_test, y_pred, target_names=target_names))


pickle.dump(
    classifier,
    open(
        Path(__file__)
        .parent.parent.joinpath("models")
        .joinpath("random_forest")
        .joinpath("random_forest.pkl"),
        "wb",
    ),
)
pickle.dump(
    cv,
    open(
        Path(__file__)
        .parent.parent.joinpath("models")
        .joinpath("random_forest")
        .joinpath("count_vectorizer_random_forest.pkl"),
        "wb",
    ),
)
