# import spacy
# from spacy.lang.de.examples import sentences
# from collections import OrderedDict
# import numpy as np

# nlp = spacy.load('de_core_news_sm')

# doc = nlp(sentences[1])
# print(doc.text)
# for token in doc:
# print(token.text, token.pos_, token.dep_)


def contains_modal_verb(sentence):
    for token in sentence:
        if token.dep_ == "aux" and token.tag_ == "MD":
            return True

    return False
