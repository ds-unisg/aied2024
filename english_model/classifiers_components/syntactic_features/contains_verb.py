def contains_verb(sentence):
    for token in sentence:
        if token.pos_ == "VERB":
            return True

    return False
