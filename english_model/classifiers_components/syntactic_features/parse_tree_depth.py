depths = {}


def root_depth(node, depth):
    depths[node.orth_] = depth
    if node.n_lefts + node.n_rights > 0:
        return [root_depth(child, depth + 1) for child in node.children]


def parse_tree_depth(doc):
    [root_depth(sent.root, 0) for sent in doc.sents]
    return max(depths.values())
