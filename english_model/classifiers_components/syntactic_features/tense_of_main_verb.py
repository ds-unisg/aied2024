def get_tense(sentence):
    for token in sentence:
        if token.pos_ == "VERB" and token.tag_ in ("VBN", "VBD"):
            return -1
        elif token.pos_ == "VERB" and token.tag_ in ("VBP", "VBZ", "VB"):
            return 1
        else:
            return 0
