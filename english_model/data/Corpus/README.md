We annotated data was created by Stab and Gurevych[1]. We randomly selected a subset from the Feedback Prize dataset[2] and modified its format to fit as an extension to Stab and Gurevych's essays.

To use the tool, please make sure that you download the essays first. 

[1] https://tudatalib.ulb.tu-darmstadt.de/handle/tudatalib/2422
[2] https://www.kaggle.com/competitions/feedback-prize-2021/data?select=train.csv