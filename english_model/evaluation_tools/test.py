import pandas as pd
import spacy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from spacy.matcher import Matcher

from english_model.classifiers_components.contextual_features import (
    shared_phrases,
    type_indicators,
)
from english_model.classifiers_components.indicators import (
    argumentative_discourse_markers,
    reference_to_first_person,
)
from english_model.classifiers_components.structural_features import (
    sentence_closes_with_question_mark,
    sentence_length,
)
from english_model.classifiers_components.syntactic_features import (
    contains_adverb,
    contains_modal_verb,
    contains_verb,
    dependency,
    ner,
    number_of_subclauses,
    parse_tree_depth,
    tense_of_main_verb,
)

nlp = spacy.load("en_core_web_sm")

# TODO operate on clausal level!


def get_label(sentence, annotations):
    for ann in annotations:
        if ann[0] in sentence:
            assigned_label = ann[1]
            return assigned_label
    return "None"


"""The following 2 functions are relevant for the contextual features"""


### This function returns all the noun chunks of a sentence - used for the introduction and conclusion
def noun_chunks(sen):
    nouns = []
    for noun in sen.noun_chunks:
        nouns.append(noun)

    return nouns


### This function returns the verb phrases of a sentence - used for the introduction and conclusion
def verb_phrases(sen):
    ### found those patterns at https://stackoverflow.com/questions/47856247/extract-verb-phrases-using-spacy
    ### maybe we can use more/less
    pattern = [
        {"POS": "VERB", "OP": "?"},
        {"POS": "ADV", "OP": "*"},
        {"POS": "AUX", "OP": "*"},
        {"POS": "VERB", "OP": "+"},
    ]

    ### instantiate a Matcher instance
    matcher = Matcher(nlp.vocab)
    matcher.add("Verb phrase", [pattern])

    ### call the matcher to find matches
    verbPhrases = [sen[start:end] for _, start, end in matcher(sen)]

    return verbPhrases


def create_dataframe(f):
    df = pd.DataFrame.from_dict(f)

    # df = pd.get_dummies(df, columns=['label'])

    mlb = MultiLabelBinarizer()
    ### unigrams to trigrams
    cv = CountVectorizer(
        max_features=1000, ngram_range=(1, 3)
    )  # min_df=0.1, max_df=0.7

    X1 = cv.fit_transform(df.text)
    print(X1.toarray())
    print(cv.get_feature_names())
    df = df.drop(columns="text")
    count_vect_df = pd.DataFrame(X1.todense(), columns=cv.get_feature_names())
    print(pd.concat([df, count_vect_df], axis=1))

    combined_df = pd.concat([df, count_vect_df], axis=1)

    return combined_df


def create_data_set(three_classes):
    f = []
    for i in range(1, 403):
        f_in = open("english_model/data/Corpus/%s.txt" % i, "r", encoding="utf-8")
        text = f_in.read()

        f_ann = open("english_model/data/Corpus/%s.ann" % i, "r")
        line = f_ann.readline()
        annotations = []
        while line:
            li = line.split("\t")
            if li[0].startswith("T"):
                annotation = li[2]
                label = li[1].split(" ")[0]

                if not three_classes:
                    if label in ("Claim", "Premise"):
                        label = "Argumentative"
                annotations.append((annotation.strip(), label))
            line = f_ann.readline()

        # for a in annotations:
        # print(a)

        doc = nlp(text)

        sentences = [sent for sent in doc.sents]

        ### calculating the average tokens of a sentence - relative for Structural Features
        total = 0

        for p in range(len(sentences)):
            total += len(sentences[p])

        avg_sent_length = total / len(sentences)

        ### to be used in some contextual features (from shared_phrases) - can also be moved away from the dataframe
        ### into the features but that would mean sending unnecessarily sending a lot of sentences
        ### relative for Contextual Features
        intro_nouns = noun_chunks(sentences[0])
        conc_nouns = noun_chunks(sentences[len(sentences) - 1])
        intro_verbs = verb_phrases(sentences[0])
        conc_verbs = verb_phrases(sentences[len(sentences) - 1])

        ### extracting the number of subclauses from the whole document - relative for Syntactic Features
        total_subclauses = number_of_subclauses.num_of_subclauses(doc)

        ### max dependency tree depth for the whole document - relative for Syntactic Features
        doc_parse_tree_depth = parse_tree_depth.parse_tree_depth(doc)

        for j in range(len(sentences)):
            features = {}

            if sentences[j] is not None:
                sen = str(sentences[j])

                features["text"] = sen

                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ STRUCTURAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                if j == 0:
                    features["is_first"] = True
                else:
                    features["is_first"] = False

                if j == len(sentences) - 1:
                    features["is_last"] = True
                else:
                    features["is_last"] = False

                features[
                    "closing_question_mark"
                ] = sentence_closes_with_question_mark.sentence_closes_with_question_marks(
                    sentences[j]
                )  # bool

                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ INDICATOR FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                features[
                    "causal_markers"
                ] = argumentative_discourse_markers.contains_causal_markers(
                    sentences[j]
                )  # bool
                features[
                    "conditional_markers"
                ] = argumentative_discourse_markers.contains_conditional_markers(
                    sentences[j]
                )  # bool
                features[
                    "adversative_markers"
                ] = argumentative_discourse_markers.contains_adversative_markers(
                    sentences[j]
                )  # bool
                features[
                    "consecutive_markers"
                ] = argumentative_discourse_markers.contains_consecutive_markers(
                    sentences[j]
                )  # bool
                features[
                    "concessive_markers"
                ] = argumentative_discourse_markers.contains_concessive_markers(
                    sentences[j]
                )  # bool
                features[
                    "first_person"
                ] = reference_to_first_person.contains_first_person(
                    sentences[j]
                )  # bool

                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ CONTEXTUAL FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                features["thesis_indicator"] = type_indicators.thesis_context(
                    sen
                )  # bool
                features["rebuttal_indicator"] = type_indicators.rebuttal_context(
                    sen
                )  # bool
                features["backward_context"] = type_indicators.backward_context(sen)
                features["forward_context"] = type_indicators.forward_context(sen)


                """ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ SYNTACTIC AND OTHER FEATURES ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ """

                features["parse_tree_depth"] = doc_parse_tree_depth  # numeric
                features["sentence_dependency_depth"] = dependency.tree_depth(
                    sentences[j]
                )
                features["number_of_subclauses"] = total_subclauses  # numeric


                f.append(features)

                # print(features)

        # print(f)

    dataframe = create_dataframe(f)

    return dataframe


### Instead of creating a new csv file, reuse the existing one in the classifiers_components package - line 291
# dataset = create_data_set(True)

### display the dimensions of the dataset
# analyse_dataset.analyse(dataset)


### save DataFrame
# dataset.to_csv('analytical_base_table.csv', index=None)

dataset = pd.read_csv("english_model/classifiers_components/analytical_base_table.csv")

### train-test split
y = dataset.loc[:, ["label"]]  # y = dataset.label
X = dataset.drop(["label"], axis=1)
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2
)  # random_state=1234

### Naive Bayes
classifier = GaussianNB()
classifier.fit(X_train, y_train.values.ravel())
y_pred = classifier.predict(X_test)
print(y_pred)
accuracy = accuracy_score(y_test, y_pred)
print("NB: ", accuracy)

### SVC
clf = LinearSVC()
clf.fit(X_train, y_train.values.ravel())
y_pred2 = clf.predict(X_test)
print(y_pred2)
accuracy2 = accuracy_score(y_test, y_pred2)
print("linear SVC: ", accuracy2)

### Decision Tree
tree = DecisionTreeClassifier(max_depth=3)  # max_depth = 2
tree.fit(X_train, y_train.values.ravel())
y_pred3 = tree.predict(X_test)
print(y_pred3)
accuracy3 = accuracy_score(y_test, y_pred3)
print("decision tree: ", accuracy3)

### Logistic Regression
logisticRegr = LogisticRegression()
logisticRegr.fit(X_train, y_train.values.ravel())
y_pred4 = logisticRegr.predict(X_test)
print(y_pred4)
accuracy4 = accuracy_score(y_test, y_pred4)
print("logistic regression: ", accuracy4)
