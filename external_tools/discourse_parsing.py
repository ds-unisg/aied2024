import os
import re
import requests

DISCOURSE_HOST = os.getenv("DISCOURSE_HOST", "http://localhost:8000")

DISCOURSE_ENDPOINT = f"{DISCOURSE_HOST}/parse"


def remove_html_tags(text):
    clean_text = re.sub('<.*?>', '', text)

    return clean_text


def clean(disourse_tree):
    clean_dict = {
        "[": "",
        "]": "",
        "(": "",
        ")": "",
        "'": "",
        ' ".,': '".',
        ' .",': '."',
        " '.,": "'.",
        " .',": ".'",
        ' "?,': '"?',
        ' ?",': '?"',
        " '?,": "'.?",
        " ?',": "?'",
        ' "!,': '"!',
        ' !",': '!"',
        " '!,": "'!",
        " !',": "!'",
        " .,": ".",
        " ?,": "?",
        " !,": "!",
        " .": ".",
        " ?": "?",
        " !": "!",
        " ,": ","
    }

    clean_discourse = disourse_tree
    for key, item in clean_dict.items():
        clean_discourse = clean_discourse.replace(key, item)

    return clean_discourse


def discourse_labels(text_input):
    clean_text_input = remove_html_tags(text_input)
    files = {"input": clean_text_input}

    response = requests.post(DISCOURSE_ENDPOINT, files=files)
    discourse_to_return = []

    if response.status_code == 200:
        discourse_list = response.text.split("ParseTree(")

        for item in discourse_list[1:]:
            item = clean(item)

            first_comma_index = item.index(",")
            label = item[:first_comma_index]
            if label[:len(label) - 2] == "Temporal":
                label = "Cause"
            else:
                label = label[:len(label) - 2]
            text = item[first_comma_index:].replace(",,", "")[2:]

            if len(text) > 2:
                discourse_to_return.append([label, text])

    return discourse_to_return

# discourse_labels("Should students be taught to compete or to cooperate? It is always said that competition can effectively promote the development of economy. In order to survive in the competition, companies continue to improve their products and service, and as a result, the whole society prospers. However, when we discuss the issue of competition or cooperation, what we are concerned about is not the whole society, but the development of an individual's whole life. From this point of view, I firmly believe that we should attach more importance to cooperation during primary education. First of all, through cooperation, children can learn about interpersonal skills which are significant in the future life of all students. What we acquired from team work is not only how to achieve the same goal with others but more importantly, how to get along with others. During the process of cooperation, children can learn about how to listen to opinions of others, how to communicate with others, how to think comprehensively, and even how to compromise with other team members when conflicts occurred. All of these skills help them to get on well with other people and will benefit them for the whole life. On the other hand, the significance of competition is that how to become more excellence to gain the victory. Hence it is always said that competition makes the society more effective. However, when we consider about the question that how to win the game, we always find that we need the cooperation. The greater our goal is, the more competition we need. Take Olympic games which is a form of competition for instance, it is hard to imagine how an athlete could win the game without the training of his or her coach, and the help of other professional staffs such as the people who take care of his diet, and those who are in charge of the medical care. The winner is the athlete but the success belongs to the whole team. Therefore without the cooperation, there would be no victory of competition. Consequently, no matter from the view of individual development or the relationship between competition and cooperation we can receive the same conclusion that a more cooperative attitudes towards life is more profitable in one's success.")

