import Levenshtein as lev


def similarity(s1, s2):
    distance = lev.distance(s1, s2)
    return distance
