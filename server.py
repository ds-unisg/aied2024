# -*- coding: utf-8 -*-
import datetime
import os
from datetime import timedelta
from functools import update_wrapper

import pyphen
import spacy

# import readability
import textstat
from flask import Flask, current_app, jsonify, make_response, render_template, request

from english_model.classifiers_components.contextual_features import (
    type_indicators as type_indicators_en,
)
from english_model.classifiers_components.predictions import (
    predict_random_forest_AAE_FP as predict_random_forest_AAE_FP_en,
)
from external_tools.chatGPT_suggestions import request_suggestions
from external_tools.discourse_parsing import discourse_labels
from transformers_models.relation_classification.deberta import (
    deberta_predict_relations,
)
from transformers_models.relation_classification.miniLM2 import (
    minilm2_predict_relations,
)
from transformers_models.sentence_classification import (
    bert_sentence_classifier as predict_Bert_en,
)
from transformers_models.sentence_classification import classification_through_relations

# from english_model.classifiers_components.predictions import predict_log_reg, predict_gaussian_nb

YOUR_URL = "" #Enter your URL here

ARTIST_URL = os.environ.get("ARTIST_URL", YOUR_URL)

def crossdomain(
    origin=None,
    methods=None,
    headers=None,
    max_age=21600,
    attach_to_all=True,
    automatic_options=True,
):
    """Decorator function that allows crossdomain requests.
    Courtesy of
    https://blog.skyred.fi/articles/better-crossdomain-snippet-for-flask.html
    """
    if methods is not None:
        methods = ", ".join(sorted(x.upper() for x in methods))

    if headers is not None and not isinstance(headers, str):
        headers = ", ".join(x.upper() for x in headers)

    if not isinstance(origin, str):
        origin = ", ".join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        """Determines which methods are allowed"""
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers["allow"]

    def decorator(f):
        """The decorator function"""

        def wrapped_function(*args, **kwargs):
            """Caries out the actual cross domain code"""
            if automatic_options and request.method == "OPTIONS":
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != "OPTIONS":
                return resp

            h = resp.headers
            h["Access-Control-Allow-Origin"] = origin
            h["Access-Control-Allow-Methods"] = get_methods()
            h["Access-Control-Max-Age"] = str(max_age)
            h["Access-Control-Allow-Credentials"] = "true"
            h[
                "Access-Control-Allow-Headers"
            ] = "Origin, X-Requested-With, Content-Type, Accept, Authorization"
            if headers is not None:
                h["Access-Control-Allow-Headers"] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator


app = Flask(__name__)
nlp_EN = spacy.load("en_core_web_sm")
dic = pyphen.Pyphen(lang="de")
# global var - saves the editor text when users change pages
write_index_saved_text = ""

HELP_TEXT = """
Sorry, but I am new here. I cannot help you at the moment.
"""

# Used for the relations heuristic function
english_german_mapping = {"contradicts": "widerspricht", "entails": "beinhaltet"}


def _server_time():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


@app.route("/", methods=["GET", "OPTIONS"])
@crossdomain(origin="*")
def home():
    return render_template(
        "write_index.html",
        content=write_index_saved_text,
        artist_url=os.getenv("ARTIST_URL"),
    )


@app.route("/demo", methods=["GET", "OPTIONS"])
@crossdomain(origin="*")
def demo():
    return render_template("demo_index.html", artist_url=os.getenv("ARTIST_URL"))


@app.route("/indicators", methods=["GET", "OPTIONS"])
@crossdomain(origin="*")
def indicators():
    return render_template("indicators_index.html", artist_url=os.getenv("ARTIST_URL"))


@app.route("/help", methods=["GET", "OPTIONS"])
@crossdomain(origin="*")
def help_page():
    return jsonify({"help": HELP_TEXT, "server_time": _server_time()})


# newly added - retains text from the write_index.html page
@app.route("/editorText", methods=["POST", "OPTIONS"])
@crossdomain(origin="*")
def editor_text():
    global write_index_saved_text
    write_index_saved_text = request.get_json().get("text").strip()
    return "", 204


@app.errorhandler(404)
def page_not_found(_):
    return make_response(jsonify({"message": "No interface defined for URL"}), 404)


@app.route("/analyze", methods=["POST", "OPTIONS"])
@crossdomain(origin="*")
def analyze():
    text = request.get_json().get("text")
    language = request.get_json().get("language").strip()
    cls = request.get_json().get("classifier").strip()

    try:
        response = process_text_input(text, language, cls)
        return jsonify(response)
    except BaseException as error:
        return make_response(jsonify({"message": "ERROR: {}".format(error)}), 400)


@app.route("/discourse", methods=["POST", "OPTIONS"])
@crossdomain(origin="*")
def discourse():
    doc = request.get_json()
    text = doc.get("text")
    language = doc.get("language").strip()

    try:
        response = {'discourse': discourse_labels(text)}
        return jsonify(response)
    except BaseException as error:
        return make_response(jsonify({"message": "ERROR: {}".format(error)}), 400)


@app.route("/chatGPT_suggestions", methods=["POST", "OPTIONS"])
@crossdomain(origin="*")
def chatGPT_suggestions():
    prompt = request.get_json().get("prompt")

    try:
        chatGPT_response = {"suggestion": request_suggestions(prompt)}
        return jsonify(chatGPT_response)
    except BaseException as error:
        return make_response(jsonify({"message": "ERROR: {}".format(error)}), 400)


def predict_components(text_input, lan, start_id, start_char, cls):
    predictions = []

    if lan == "English":
        if cls == "random_tree":
            predictions = predict_random_forest_AAE_FP_en.predict_AAE_FP(text_input)
        elif cls in ["bert_deberta_no_contra", "bert_deberta_no_contra_chain"]:
            predictions = predict_Bert_en.predict_bert(text_input)
        elif cls in ["deberta_2x", "deberta_2x_chain"]:
            predictions = classification_through_relations.predict_through_relations(text_input, "deberta")
        elif cls == "miniLM2_2x_chain":
            predictions = classification_through_relations.predict_through_relations(text_input, "miniLM2")

        doc = nlp_EN(text_input)
        sentences = [sent.text for sent in doc.sents]

        count = 0
        id_count = start_id + 1
        elements = []
        argumentative_length = 0.0
        non_argumentative_length = 0.0

        for p in predictions:
            if p == "Claim":
                argumentative_length = argumentative_length + len(
                    [t for t in nlp_EN(sentences[count])]
                )
                elements.append(
                    {
                        "id": id_count,
                        "label": "claim",
                        "start": text_input.find(sentences[count]) + start_char,
                        "length": len(sentences[count]),
                        "text": sentences[count],
                        "confidence": 1.0,
                    }
                )

            elif p == "Premise":
                argumentative_length = argumentative_length + len(
                    [t for t in nlp_EN(sentences[count])]
                )
                elements.append(
                    {
                        "id": id_count,
                        "label": "premise",
                        "start": text_input.find(sentences[count]) + start_char,
                        "length": len(sentences[count]),
                        "text": sentences[count],
                        "confidence": 1.0,
                    }
                )
            elif p == "MajorClaim":
                argumentative_length = argumentative_length + len(
                    [t for t in nlp_EN(sentences[count])]
                )
                elements.append(
                    {
                        "id": id_count,
                        "label": "majorClaim",
                        "start": text_input.find(sentences[count]) + start_char,
                        "length": len(sentences[count]),
                        "text": sentences[count],
                        "confidence": 1.0,
                    }
                )

            else:
                non_argumentative_length = non_argumentative_length + len(
                    [t for t in nlp_EN(sentences[count])]
                )

            count += 1
            id_count += 1

        return elements, argumentative_length, non_argumentative_length


"""
    English readability index - handled with a library
"""


def get_readability(text):
    return textstat.flesch_reading_ease(text)





def get_number_of_characters(text):
    return len(text)


def get_number_of_sentences(text, lan):
    sentences = []
    if lan == "English":
        doc = nlp_EN(text)
        sentences = [sent.text for sent in doc.sents]

    return len(sentences)


def count_syllables(token):
    split_token = dic.inserted(token)
    syllables = split_token.split("-")
    return len(syllables)



"""
    English discourse markers
"""


def get_discourse_coherence_EN(text):
    causal_markers = ["since", "because", "as"]
    consecutive_markers = ["thus", "thereby", "therefore"]
    adversative_markers = [
        "instead of",
        "otherwise",
        "on the other hand",
        "on the one hand",
        "however",
        "but",
        "rather",
        "in turn",
    ]
    concessive_markers = [
        "however",
        "nevertheless",
        "although",
        "even though",
        "whereby",
        "though",
    ]
    conditional_markers = ["if", "whether", "provided", "when"]

    argumentative_discourse_markers = (
        causal_markers
        + consecutive_markers
        + adversative_markers
        + concessive_markers
        + conditional_markers
    )

    doc = nlp_EN(text)
    tokens = [token.text.lower() for token in doc]

    counter = 0
    for t in tokens:
        if t in argumentative_discourse_markers:
            counter += 1

    sentences = [sent.text for sent in doc.sents]

    return counter, sentences




def do_analyze(text, lan, sentence_count, char_count, cls):
    scores = {}

    # elements
    elements, argumentative_length, non_argumentative_length = predict_components(
        text, lan, sentence_count, char_count, cls
    )
    elements = sorted(elements, key=lambda e: e["start"])

    if cls == "random_tree":
        relations = relations_heuristic_context_pilot(elements, lan)
    elif cls in ["bert_deberta_no_contra", "deberta_2x"]:
        relations = relations_heuristic_context_transformers(
            elements, lan, ["neutral", "contradicts"]
        )
    elif cls in ["bert_deberta_no_contra_chain", "deberta_2x_chain"]:
        relations = relations_heuristic_context_transformers_chain(
            elements, lan, "deberta", ["neutral", "contradicts"]
        )
    elif cls == "miniLM2_2x_chain":
        relations = relations_heuristic_context_transformers_chain(
            elements, lan, "minilm2", ["neutral", "contradicts"]
        )

    # unsupported relations
    number_of_claims, number_of_unsupported_claims = unsupported_claims(
        elements, relations
    )

    # scores
    if lan == "English":
        score_argumentative = argumentative_length / (
            argumentative_length + non_argumentative_length
        )

        if len(get_discourse_coherence_EN(text)[1]) - 1 > 0:
            score_discourse = get_discourse_coherence_EN(text)[0] / (
                len(get_discourse_coherence_EN(text)[1]) - 1
            )
        else:
            score_discourse = 0

        score_readability = get_readability(text) / 100

        if number_of_claims > 0:
            score_unsupported = number_of_unsupported_claims / number_of_claims
        else:
            score_unsupported = 0

        scores["argumentative"] = {
            "description": "Argumentative",
            "score": score_argumentative,
            "details": argumentative_text(score_argumentative, lan),
        }
        scores["helpful"] = {
            "description": "Readability",
            "score": score_readability,
            "details": readability_text(score_readability, lan),
        }
        # scores['structure'] = {'description': 'Structure', 'score': .33, 'details': structure_text(lan)}
        scores["discourse"] = {
            "description": "Coherence",
            "score": score_discourse,
            "details": discourse_text(score_discourse, lan),
        }
        scores["unsupported"] = {
            "description": "Persuasiveness",
            "score": 1 - score_unsupported,
            "details": unsupported_text(score_unsupported, lan),
        }


    elements_dict = {
        "elements": elements,
        "relations": relations,
        "scores": scores,
    }

    return elements_dict


def unsupported_claims(elements, relations):
    number_of_claims = 0
    number_of_unsupported_claims = 0
    for e in elements:
        if e["label"] == "claim" or e["label"] == "majorClaim":
            number_of_claims += 1
            supported = False

            for r in relations:
                if r["srcElem"] == e["id"] or r["trgElem"] == e["id"]:
                    supported = True
                    break
            if not supported:
                number_of_unsupported_claims += 1

    return number_of_claims, number_of_unsupported_claims


def relations_heuristic_context_transformers_chain(
    elements, lan, relation_classifier, disallowed_labels
):
    temp_relations = []

    if relation_classifier == "deberta":
        rel_cls = deberta_predict_relations
    elif relation_classifier == "minilm2":
        rel_cls = minilm2_predict_relations

    if lan == "English":
        for i in range(len(elements)):
            if elements[i]["label"] != "neutral":
                for j in range(len(elements)):
                    if elements[j]["label"] != "neutral" and j != i:
                        label = rel_cls(elements[i]["text"], elements[j]["text"])
                        if label not in disallowed_labels:
                            temp_relations.append(
                                {
                                    "srcElem": elements[i]["id"],
                                    "trgElem": elements[j]["id"],
                                    "label": label,
                                    "confidence": 1.0,
                                }
                            )

    relations = []
    for val in temp_relations:
        if val not in relations:
            relations.append(val)

    return relations


def relations_heuristic_context_transformers(
    elements, lan, disallowed_labels
):
    temp_relations = []

    # used for transformers_models
    major_claims_list = []
    claims_list = []
    premises_list = []

    """
        Creating a list for major claims, claims and premises. Each list contains:
        - major_claims list - a tuple with two elements:
            1. The major claim as is in the elements list
            2. A dict with two values:
                + 'previous': the claim that is preceding this major claim (None if not existing)
                + 'next': the claim that is following this major claim (None if not existing)

        - claims list - a tuple with two elements:
            1. The claim as is in the elements list
            2. A dict with two values:
                + 'prev_premise': the premise that is preceding this claim (None if not existing)
                + 'next_premise': the premise that is following this claim (None if not existing)
                + 'prev_major_claim': the major claim preceding this claim (None if not existing)
                + 'next_major_claim': the major claim following this claim (None if not existing)

        - premises list - a tuple with two elements:
            1. The premise as is in the elements list
            2. A dict with two values:
                + 'previous': the claim or major claim that is preceding this premise (None if not existing)
                + 'next': the claim or major claim that is following this premise (None if not existing)
    """

    for i in range(len(elements)):
        ### Major Claims
        if elements[i]["label"] == "majorClaim":
            major_claims_list.append(elements[i])

        ### Claims
        if elements[i]["label"] == "claim":
            claims_list.append(elements[i])

        ### Premises
        elif elements[i]["label"] == "premise":
            premises_list.append(elements[i])

    """ 
        Will contain all the used premises from the first iteration (see below) and exclude them from the second
        iteration (see below).
        If this is not done, a complex web of relations may be created for some documents (maybe we can use it with
        more complex relations).
    """
    # used_premises_ids = []

    if lan == "English":
        """
        Iterating over all claim for each major claim in major_claims_list

        """

        for majorClaim in major_claims_list:
            for claim in claims_list:
                label = deberta_predict_relations(majorClaim["text"], claim["text"])

                if label not in disallowed_labels:
                    temp_relations.append(
                        {
                            "srcElem": claim["id"],
                            "trgElem": majorClaim["id"],
                            "label": label,
                            "confidence": 1.0,
                        }
                    )

        """ 
            Iterating over all premises and major claims for each claim in claims_list
        """

        for claim in claims_list:
            for majorClaim in major_claims_list:
                label = deberta_predict_relations(claim["text"], majorClaim["text"])

                if label not in disallowed_labels:
                    temp_relations.append(
                        {
                            "srcElem": claim["id"],
                            "trgElem": majorClaim["id"],
                            "label": label,
                            "confidence": 1.0,
                        }
                    )

            for premise in premises_list:
                label = deberta_predict_relations(claim["text"], premise["text"])

                if label not in disallowed_labels:
                    temp_relations.append(
                        {
                            "srcElem": premise["id"],
                            "trgElem": claim["id"],
                            "label": label,
                            "confidence": 1.0,
                        }
                    )
        """
            Iterating over all claims for each premise in the premises_list
        """

        for premise in premises_list:
            for claim in claims_list:
                label = deberta_predict_relations(premise["text"], claim["text"])

                if label not in disallowed_labels:
                    temp_relations.append(
                        {
                            "srcElem": premise["id"],
                            "trgElem": claim["id"],
                            "label": label,
                            "confidence": 1.0,
                        }
                    )

    """ Check to remove duplicate relations """
    relations = []
    for val in temp_relations:
        if val not in relations:
            relations.append(val)

    return relations


def relations_heuristic_context_pilot(elements, lan):
    temp_relations = []
    major_claims = []
    claims = []
    premises = []

    """
        Creating a list for major claims, claims and premises. Each list contains:
        - major_claims list - a tuple with two elements:
            1. The major claim as is in the elements list
            2. A dict with two values:
                + 'previous': the claim that is preceding this major claim (None if not existing)
                + 'next': the claim that is following this major claim (None if not existing)

        - claims list - a tuple with two elements:
            1. The claim as is in the elements list
            2. A dict with two values:
                + 'prev_premise': the premise that is preceding this claim (None if not existing)
                + 'next_premise': the premise that is following this claim (None if not existing)
                + 'prev_major_claim': the major claim preceding this claim (None if not existing)
                + 'next_major_claim': the major claim following this claim (None if not existing)

        - premises list - a tuple with two elements:
            1. The premise as is in the elements list
            2. A dict with two values:
                + 'previous': the claim or major claim that is preceding this premise (None if not existing)
                + 'next': the claim or major claim that is following this premise (None if not existing)
    """

    for i in range(len(elements)):
        ### Major Claims
        if elements[i]["label"] == "majorClaim":
            prev_claim = None
            next_claim = None

            for elem in elements[i::-1]:
                if elem["label"] == "claim":
                    prev_claim = elem["id"]
                    break

            for elem in elements[i:]:
                if elem["label"] == "claim":
                    next_claim = elem["id"]
                    break

            relative_premises = {"previous": prev_claim, "next": next_claim}
            major_claims.append((elements[i], relative_premises))

        ### Claims
        if elements[i]["label"] == "claim":
            prev_premise = None
            next_premise = None
            prev_major_claim = None
            next_major_claim = None

            for elem in elements[i::-1]:
                if elem["label"] == "premise":
                    prev_premise = elem["id"]
                    break
                elif elem["label"] == "majorClaim":
                    prev_major_claim = elem["id"]
                    break

            for elem in elements[i:]:
                if elem["label"] == "premise":
                    next_premise = elem["id"]
                    break
                elif elem["label"] == "majorClaim":
                    next_major_claim = elem["id"]
                    break

            relative_premises = {
                "prev_premise": prev_premise,
                "next_premise": next_premise,
                "prev_major_claim": prev_major_claim,
                "next_major_claim": next_major_claim,
            }
            claims.append((elements[i], relative_premises))

        ### Premises
        elif elements[i]["label"] == "premise":
            prev_claim = None
            next_claim = None

            for elem in elements[i::-1]:
                if elem["label"] == "claim":
                    prev_claim = elem["id"]
                    break

            for elem in elements[i:]:
                if elem["label"] == "claim":
                    next_claim = elem["id"]
                    break

            relative_claims = {"previous": prev_claim, "next": next_claim}
            premises.append((elements[i], relative_claims))

    """ 
        Will contain all the used premises from the first iteration (see below) and exclude them from the second
        iteration (see below).
        If this is not done, a complex web of relations may be created for some documents (maybe we can use it with
        more complex relations).
    """
    # used_premises_ids = []

    if lan == "English":
        """
        Iterating over all major claims. If the type indicators bellow exist in each major claim, then a claim
        will be assigned to the major claim in the relations list. Overview of the iteration:

        - If forward context exists in major claim:
            + Assign the following claim as a supporting element

        - If backward, rebuttal or thesis context exists in claim:
            + Assign the preceding claim as a supporting element
        """

        for majorClaim in major_claims:
            if forward_context_en(majorClaim[0]["text"]):
                if majorClaim[1]["next"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": majorClaim[1]["next"],
                            "trgElem": majorClaim[0]["id"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )

            if (
                backward_context_en(majorClaim[0]["text"])
                or rebuttal_context_en(majorClaim[0]["text"])
                or thesis_context_en(majorClaim[0]["text"])
            ):
                if majorClaim[1]["previous"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": majorClaim[1]["previous"],
                            "trgElem": majorClaim[0]["id"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )

        """ 
            Iterating over all claims. If the type indicators bellow exist in each claim, then a premise will
            be assigned to the claim in the relations list. Overview of the iteration:

            - If forward context exists in claim:
                + Assign the premise that follows as a supporting element
                + Assign this claim as support to the following major claim
            - If forward, backward or rebuttal context exists in claim:
                + Assign the preceding premise as a supporting element
                + Assign this claim as support to the preceding major claim
            - In any other case:
                + Assign this claim as support to the preceding major claim
        """

        for claim in claims:
            if forward_context_en(claim[0]["text"]):
                if claim[1]["next_premise"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": claim[1]["next_premise"],
                            "trgElem": claim[0]["id"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )
                    # used_premises_ids.append(claim[1]['next_premise'])

                if claim[1]["next_major_claim"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": claim[0]["id"],
                            "trgElem": claim[1]["next_major_claim"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )

            if (
                backward_context_en(claim[0]["text"])
                or rebuttal_context_en(claim[0]["text"])
                or thesis_context_en(claim[0]["text"])
            ):
                if claim[1]["prev_premise"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": claim[1]["prev_premise"],
                            "trgElem": claim[0]["id"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )
                    # used_premises_ids.append(claim[1]['prev_premise'])
                if claim[1]["prev_major_claim"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": claim[0]["id"],
                            "trgElem": claim[1]["prev_major_claim"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )

            else:
                if claim[1]["prev_major_claim"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": claim[0]["id"],
                            "trgElem": claim[1]["prev_major_claim"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )

        """
            Iterating over the previously unused premises. For each premise that has not yet been used, the following
            rules apply:

            - If forward context exists in the premise:
                + The next claim is assigned a relation with this premise
            - If backward, rebuttal or a combination of thesis context and a relative position <0.8 out of 1:
                + The previous claim is assigned a relation with this premise
            - If thesis context exists and the relative position is >= 0.8:
                + The next claim is assigned a relation with this premise
            - In any other case:
                + The current premise is assigned to the previous claim

            (For points 2 and 3, the relative position is calculated by dividing the index pf the current element in
            the elements list by the length of the elements list. This is done to account for premises that are 
            close to the end of the document supporting the conclusion.)
        """

        for premise in premises:
            # if premise[0]['id'] not in set(used_premises_ids):
            if forward_context_en(premise[0]["text"]):
                if premise[1]["next"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": premise[0]["id"],
                            "trgElem": premise[1]["next"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )

            if (
                backward_context_en(premise[0]["text"])
                or rebuttal_context_en(premise[0]["text"])
                or thesis_context_en(premise[0]["text"])
            ):
                if premise[1]["previous"] is not None:
                    temp_relations.append(
                        {
                            "srcElem": premise[0]["id"],
                            "trgElem": premise[1]["previous"],
                            "label": "supports",
                            "confidence": 1.0,
                        }
                    )

            if premise[1]["previous"] is not None:
                temp_relations.append(
                    {
                        "srcElem": premise[0]["id"],
                        "trgElem": premise[1]["previous"],
                        "label": "supports",
                        "confidence": 1.0,
                    }
                )

    """ Check to remove duplicate relations """
    relations = []
    for val in temp_relations:
        if val not in relations:
            relations.append(val)

    return relations


def forward_context_en(sentence):
    return type_indicators_en.forward_context(sentence)


def backward_context_en(sentence):
    return type_indicators_en.backward_context(sentence)


def rebuttal_context_en(sentence):
    return type_indicators_en.rebuttal_context(sentence)


def thesis_context_en(sentence):
    return type_indicators_en.thesis_context(sentence)



def process_text_input(in_text, lan, cls):
    return do_analyze(in_text.replace("\n", " "), lan, 0, 0, cls)


def concat_results(responses, lan):
    temp_elements = []
    temp_relations = []
    temp_scores = {}
    score_argumentative = 0
    score_readability = 0  # key = helpful
    score_coherence = 0  # key = discourse
    score_persuasiveness = 0  # key = unsupported

    for result in responses:
        for element in result["elements"]:
            temp_elements.append(element)

        for relation in result["relations"]:
            temp_relations.append(relation)

        score_argumentative += result["scores"]["argumentative"]["score"] * len(
            result["elements"]
        )
        score_readability += result["scores"]["helpful"]["score"] * len(
            result["elements"]
        )
        score_coherence += result["scores"]["discourse"]["score"] * len(
            result["elements"]
        )
        score_persuasiveness += result["scores"]["unsupported"]["score"] * len(
            result["elements"]
        )

    if len(temp_elements) > 0:
        score_argumentative = score_argumentative / len(temp_elements)
        score_readability = score_readability / len(temp_elements)
        score_coherence = score_coherence / len(temp_elements)
        score_persuasiveness = score_persuasiveness / len(temp_elements)
    else:
        score_argumentative = 0
        score_readability = 0
        score_coherence = 0
        score_persuasiveness = 0

    if lan == "English":
        temp_scores["argumentative"] = {
            "description": "Argumentative",
            "score": score_argumentative,
            "details": argumentative_text(score_argumentative, lan),
        }
        temp_scores["helpful"] = {
            "description": "Readability",
            "score": score_readability,
            "details": readability_text(score_readability, lan),
        }
        temp_scores["discourse"] = {
            "description": "Coherence",
            "score": score_coherence,
            "details": discourse_text(score_coherence, lan),
        }
        temp_scores["unsupported"] = {
            "description": "Persuasiveness",
            "score": score_persuasiveness,
            "details": unsupported_text(score_persuasiveness, lan),
        }

    return {
        "elements": temp_elements,
        "relations": temp_relations,
        "scores": temp_scores,
        # 'learningProgress': 0.33
    }


# Not used
def structure_text(lan):
    if lan == "English":
        return "Try to improve your argumentation structure next time."


def argumentative_text(score, lan):
    text = ""
    if lan == "English":
        if score <= 0.4:
            text = "You need to keep working on your argumentation structure. Try writting more Premises, Claims and Major Claims to improve the argumentation structure of your text."
        elif score <= 0.8:
            text = "There is structure in your argumentation, but further improvements can be made. Try supporting unsupported Claims with Premises and Major Claims with Claims to improve your argumentation structure."
        else:
            text = "There is a strong argumentation structure in your text."

    return text


def readability_text(score, lan):
    text = ""
    if lan == "English":
        if score <= 0.4:
            text = "Try to form shorter sentences and use simpler words to increase the readability of your text."
        elif score <= 0.8:
            text = "The readability of your text is good. Nevertheless, try to formulate your statements more clearly and to form short concise sentences to improve the readability and thus the comprehensibility of your text even further."
        else:
            text = "Your text is easy to read."

    return text


def discourse_text(score, lan):
    text = ""
    if lan == "English":
        if score <= 0.4:
            text = "Coherence is measured by the recognized links between your claims and your premises. Try to find examples, facts, or explanations to support each claim. Use connecting words (so-called connectors) to increase the coherence of your text. Click Indicators/Indicatoren in the Navigation bar to see a comprehensive list of connectors that Artist uses."
        elif score <= 0.8:
            text = "The coherence of your text is good. Your statements are generally supported by premises. To further increase coherence, try using clearer connecting words between statements."
        else:
            text = "Your text is well coherent."

    return text


def unsupported_text(score, lan):
    text = ""
    if lan == "English":
        if score <= 0.4:
            text = "The persuasiveness of your text is measured by the proportion of supported arguments in your text. Try to add more arguments and support them with premises to make your text more convincing."
        elif score <= 0.8:
            text = "Your text is very persuasive. Try to pick up potential counter-arguments and refute them to make your text even more convincing."
        else:
            text = "Your text is highly persuasive."

    return text


if __name__ == "__main__":
    DEFAULT_HOST = "0.0.0.0"  # 127.0.0.1
    DEFAULT_PORT = "5130"

    host = DEFAULT_HOST
    port = DEFAULT_PORT
    print("Trying to start server at '{}':'{}'".format(host, port))

    app.run(host="0.0.0.0", port=port, debug=False, use_reloader=False)
