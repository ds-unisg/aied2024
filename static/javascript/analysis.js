function analyze() {
    if (editorUpdating) {
      return;
    }
    editorUpdating = true;
    document.getElementById("editorspinner").style.display = "inline-block";
    document.getElementById("editorsubmit").style.disabled = true;
    // switch tab to Argumentation Dashboard
    openTab(event, 'tab_argumentation_dashboard');

    var editor = document.getElementById("editor");

    editor.innerHTML = editor.innerHTML.replace("<div><br></div>", "<br>");
    editor.innerHTML = editor.innerHTML.replace("</>div>", "");

    var value = editor.innerText;

    value = value.replace("\xa0", "");

    $.ajax({
      type: "POST",
      url: artist_URL.concat("analyze"),
      data: JSON.stringify({ "text": value, "language": language, "classifier": classifier_selection }),
      success: function(json){
        analysisResult = json;

        // UPDATE
        editorUpdate(editor, value);

        produceFeedback(
            language,
            analysisResult['scores']['argumentative']['score'],
            analysisResult['scores']['helpful']['score'],
            analysisResult['scores']['discourse']['score'],
            analysisResult['scores']['unsupported']['score']
        );

        for (var k in barCharts) {
          barUpdate(k);
        }
        selectedClaim = null;
        detailedGraphUpdate();
        structureGraphUpdate();
        //arguProgressUpdate();

        editorUpdating = false;
        document.getElementById("editorspinner").style.display = "none";
        document.getElementById("editorsubmit").style.disabled = false;

        if ((!analysisResult) || (analysisResult.elements.length <= 0)) {
          noArgumentationAlert();
        }

      },
      error: function (request, status, error) {
        console.log(request.responseText);
        editorUpdating = false;
        document.getElementById("editorspinner").style.display = "none";
        document.getElementById("editorsubmit").style.disabled = false;

        noArgumentationAlert();
      },
      dataType: "json",
      contentType : "application/json"
    });
  }


// Discourse
function discourse() {
    if (editorDiscourseUpdating) {
      return;
    }
    editorDiscourseUpdating = true;
    document.getElementById("editorspinner-discourse").style.display = "inline-block";
    document.getElementById("editorsubmit-discourse").style.disabled = true;
    // switch tab to Discourse Structure
    openTab(event, 'tab_discourse')

    var editor = document.getElementById("editor");

    // editor.innerText takes <br> and <div>...</div> as new line tokens. Bellow, only the <br> tag is kept.
    editor.innerHTML = editor.innerHTML.replace("<div><br></div>", "<br>");
    editor.innerHTML = editor.innerHTML.replace("<div>", "<br>");
    editor.innerHTML = editor.innerHTML.replace("</>div>", "");

    var value = editor.innerText;

    value = value.replace("\xa0", "");

    $.ajax({
      type: "POST",
      url: artist_URL.concat("discourse"),
      data: JSON.stringify({ "text": value, "language": language }),
      success: function(json){
        analysisResult = json;

        discourse_feedback(analysisResult['discourse'])

        editorDiscourseUpdating = false;
        document.getElementById("editorspinner-discourse").style.display = "none";
        document.getElementById("editorsubmit-discourse").style.disabled = false;

        if (!analysisResult) {
          noArgumentationAlert();
        }

      },
      error: function (request, status, error) {
        console.log(request.responseText);
        editorDiscourseUpdating = false;
        document.getElementById("editorspinner-discourse").style.display = "none";
        document.getElementById("editorsubmit-discourse").style.disabled = false;

        noArgumentationAlert(); 
      },
      dataType: "json",
      contentType : "application/json"
    });
}


// ChatGPT
  function chatGPT_process_response() {
    if (chatGPTResponseUpdating) {
        return;
    }

    var response_box = document.getElementById("chat-GPT-response");
    var editor = document.getElementById("editor");

    chatGPTResponseUpdating = true;
    document.getElementById("chat-GPT-spinner").style.display = "inline-block";
    document.getElementById("chat-GPT-submit").style.disabled = true;

    var value = editor.innerText;

    value = value.replace("\xa0", "");

    var prompt =
        "Please give two short suggestions for improving the argumentative quality of the following Essay: ".concat(value);

    $.ajax({
      type: "POST",
      url: artist_URL.concat("chatGPT_suggestions"),
      data: JSON.stringify({ "prompt": prompt }),
      success: function(json){
        chatGPT_response = json;

        response_box.innerHTML = chatGPT_response['suggestion']

        chatGPTResponseUpdating = false;
        document.getElementById("chat-GPT-spinner").style.display = "none";
        document.getElementById("chat-GPT-submit").style.disabled = false;
      },
      error: function (request, status, error) {
        console.log(request.responseText);
        chatGPTResponseUpdating = false;
        document.getElementById("chat-GPT-spinner").style.display = "none";
        document.getElementById("chat-GPT-submit").style.disabled = false;

        noChatGPTResponseAlert(); 
      },
      dataType: "json",
      contentType : "application/json"
    });
}

// Llama 2
function llama_process_response() {
  if (llamaResponseUpdating) {
      return;
  }

  var response_box = document.getElementById("llama-response");
  var editor = document.getElementById("editor");

  llamaResponseUpdating = true;
  document.getElementById("llama-spinner").style.display = "inline-block";
  document.getElementById("llama-submit").style.disabled = true;

  var value = editor.innerText;

  value = value.replace("\xa0", "");

  var prompt = "[INST] You are an argumentation expert and an experienced teacher that loves to give helpful and encouraging advice to students. You always respond in short, concise, well-formed sentences, and you are also creative. You receive the student's text from me, which has already been analyzed with Discourse Structure Analysis. Referring to very specific elements of the text, you give the student two specific tips on what they could improve about the text in terms of argumentation. Please try to be as specific and supportive as possible giving two formative and instructive feedbacks and nothing more in 2-3 sentences. Here is the text: ".concat(value, "[/INST]")

  your_auth_token = {"Authorization": "YOUR BEARER"}
  your_api_url = 'YOUR API URL'
  $.ajax({
    type: "POST",
    url: your_api_url,
    headers: your_auth_token,
    data: JSON.stringify({ "prompt": prompt }),
    success: function(json){
      llama_response = json;

      response_box.innerHTML = llama_response['response']

      llamaResponseUpdating = false;
      document.getElementById("llama-spinner").style.display = "none";
      document.getElementById("llama-submit").style.disabled = false;
    },
    error: function (request, status, error) {
      console.log(request.responseText);
      llamaResponseUpdating = false;
      document.getElementById("llama-spinner").style.display = "none";
      document.getElementById("llama-submit").style.disabled = false;

      nollamaResponseAlert(); 
    },
    dataType: "json",
    contentType : "application/json"
  });
}
