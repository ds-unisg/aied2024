function produceFeedback(lan, scoreArgumentative, scoreReadability, scoreCoherence, scorePersuasiveness) {

    if (language == 'English') {
      if (((0.25 * scoreArgumentative) + (0.25 * scoreReadability) + (0.25 * scoreCoherence) + (0.25 * scorePersuasiveness)) <= 0.4) {
        document.getElementById('grade-feedback').innerHTML = "Significant improvements can be made in the text. Click 'Details' under each score above to view some tips on how the text can be improved."
      } else if (((0.25 * scoreArgumentative) + (0.25 * scoreReadability) + (0.25 * scoreCoherence) + (0.25 * scorePersuasiveness)) <= 0.8) {
        document.getElementById('grade-feedback').innerHTML = "The text is quite strong across all score categories, but further improvements can be made. Click 'Details' under each score above to view some tips on how the text can be improved."
      } else {
        document.getElementById('grade-feedback').innerHTML = "The text is strongly argumentative and comprehensive."
      }
    }
}

function discourse_feedback(data) {
    var element = document.getElementById('discourse-feedback');
    var content = '';

    if (data.length === 0) {
      element.innerHTML = "Sorry, the discourse parsing element seems to be unavailable at the moment";
      return;
    }

    var colorValues = [
        "Elaboration",
        "Background",
        "Preparation",
        "Contrast",
        "Attribution",
        "Enablement",
        "Explanation",
        "Evaluation",
        "Cause"
    ];

    const color_dict = {
        'Elaboration': 'elaborationColor',
        'Background': 'backgroundColor',
        'Preparation': 'preparationColor',
        'Contrast': 'contrastColor',
        'Attribution': 'attributionColor',
        'Enablement': 'enablementColor',
        'Explanation': 'explanationColor',
        'Evaluation': 'evaluationColor',
        'Cause': 'temporalColor',
        'Default': 'defaultColor'
    }

    for (var i = 0; i < data.length; i++) {
      var label = data[i][0];
      var text = data[i][1];

      // Define color code based on labels
      if (colorValues.includes(label)) {
          var color = color_dict[label];
      } else {
          var color = color_dict['Default'];
          label = "Default";
      }
      // Format and append content
        content += '<span> <b class=' + color + '>' + label + ':</b> ' + text + '</span><br>';
          //'<span style="color: ' + color + ';">' + '<span class="label">' + label + ': </span>' + ': </span>' + text + '<br>';
    }

    element.innerHTML = content;
  }

//   colors sentences based on label 
function editorUpdate(editor, value) {
    if (analysisResult) {

      // highlight elements in editor
      var innerHTML = value;
      var mapping = [];
      for (var i = 0; i < analysisResult.elements.length; i++) {
        var start = mappingIdx(mapping, analysisResult.elements[i].start);
        var end = mappingIdx(mapping, analysisResult.elements[i].start + analysisResult.elements[i].length);
        var elemId = analysisResult.elements[i].id;
        var label = analysisResult.elements[i].label;

        if (label === "claim" || label === "premise" || label === "majorClaim") {
          if (label === "claim") {
            var prefix = "<span class=\"" + label + "\" onclick=\"onEditorClaimClick(this)\" data-label=\"" + label + "\" data-id=\"" + elemId + "\" onMouseOver=\"this.style.cursor='pointer'\">";
            var suffix = "</span>";
          }
          if (label === "premise") {
            var prefix = "<span class=\"" + label + "\" data-label=\"" + label + "\" data-id=\"" + elemId + "\">";
            var suffix = "</span>";
          }
          if (label === "majorClaim") {
            var prefix = "<span class=\"" + label + "\" onclick=\"onEditorClaimClick(this)\" data-label=\"" + label + "\" data-id=\"" + elemId + "\" onMouseOver=\"this.style.cursor='pointer'\">";
            var suffix = "</span>";
          }

          innerHTML = innerHTML.substring(0, start) + prefix + innerHTML.substring(start, end) + suffix + innerHTML.substring(end);
          mappingInsert(mapping, analysisResult.elements[i].start, prefix.length);
          mappingInsert(mapping, analysisResult.elements[i].start + analysisResult.elements[i].length, suffix.length);
        }
      }

      innerHTML = innerHTML.replace(/(?:\n)/g, '<br/>');
      editor.innerHTML = innerHTML;
    }
}

// colors "Readability", "Coherence", "Persuasiveness", "Argumentative" bars for argumentation structure after analysis
function barUpdate(key) {

    // intitialize if not available
    if (!('chart' in barCharts[key])) {
      elementId = barCharts[key]['element'];
      var d = document.getElementById(elementId);
      var p = d.parentNode;
      var new_d = document.getElementById("scoreBarChartTemplate").cloneNode(true);

      p.replaceChild(new_d, d);

      new_d.removeAttribute('id');
      new_d.style.display='block';

      barCharts[key]['chart'] = new_d;
    }

    var chart = barCharts[key]['chart'];

    if (analysisResult) {
      var score = analysisResult.scores[key].score;
      var score2 = (score * 100).toFixed(0)

      var modal_id = key + '-details-modal';
      chart.querySelectorAll("[name='modal']")[0].setAttribute('id', modal_id);
      if (language == 'English') {
        chart.querySelectorAll("[name='modal-title']")[0].innerHTML = 'Details to "' + analysisResult.scores[key].description + '"';
        chart.querySelectorAll("[name='modal-footer']")[0].innerHTML = '<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>';
      } else if (language == 'Deutsch') {
        chart.querySelectorAll("[name='modal-title']")[0].innerHTML = 'Details zu "' + analysisResult.scores[key].description + '"';
        chart.querySelectorAll("[name='modal-footer']")[0].innerHTML = '<button type="button" class="btn btn-danger" data-dismiss="modal">Schliessen</button>';
      }
      chart.querySelectorAll("[name='modal-body']")[0].innerHTML = analysisResult.scores[key].details;
      chart.querySelectorAll("[name='description']")[0].innerHTML = analysisResult.scores[key].description;
      chart.querySelectorAll("[name='details']")[0].innerHTML = 'Details';
      chart.querySelectorAll("[name='details']")[0].setAttribute('href', '#' + modal_id);
      chart.querySelectorAll("[name='details']")[0].setAttribute('data-target', '#' + modal_id);
      chart.querySelectorAll("[name='bar']")[0].setAttribute('style', 'width: ' + score2 + '%');

      if (score2 <= 40) {
        chart.querySelectorAll("[name='bar']")[0].setAttribute('class', 'progress-bar-grad-40');
      } else if (score2 <= 80) {
        chart.querySelectorAll("[name='bar']")[0].setAttribute('class', 'progress-bar-grad-80');
      } else {
        chart.querySelectorAll("[name='bar']")[0].setAttribute('class', 'progress-bar-grad-100');
      }

    } else {
      chart.querySelectorAll("[name='description']")[0].innerHTML = '';
      chart.querySelectorAll("[name='details']")[0].innerHTML = '';
      chart.querySelectorAll("[name='details']")[0].setAttribute('href', '#');
      chart.querySelectorAll("[name='details']")[0].setAttribute('data-target', '#');
      chart.querySelectorAll("[name='bar']")[0].setAttribute('style', 'width: 0%');
    }
}