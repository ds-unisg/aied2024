function getElemGraph(elemId) {
    var nodes = [];
    var edges = [];

    if (analysisResult) {

      var checks = [{'level': 0, 'id': elemId}];
      var processed = [];
      while (checks.length > 0) {
        var check = checks.pop();
        var elem = argElementById(check.id);
        var level = check.level;
        var borderColor = 'black';
        var backgroundColor = 'lightblue';
        if (elem.label == 'premise') {
          backgroundColor = 'gold';
        } else if (elem.label == 'majorClaim') {
          backgroundColor = 'orange';
        }

        if (!processed.includes(elem.id)) {
          nodes.push({id: elem.id, label: elem.text, level: level, color: {border: borderColor, background: backgroundColor}});


          var incoming = getRelations(elem.id, true, false);
          var outgoing = getRelations(elem.id, false, true);

          // continue with incoming elements
          for (var i = 0; i < incoming.length; ++i) {
            edges.push({from: incoming[i].srcElem, to: incoming[i].trgElem, label: incoming[i].label});

            var neighbourId = incoming[i].srcElem;
            if (!processed.includes(neighbourId)) {
              checks.push({'level': level + 1, 'id': neighbourId});
            }
          }

          // continue with outgoing elements
          for (var i = 0; i < outgoing.length; ++i) {
            edges.push({from: outgoing[i].srcElem, to: outgoing[i].trgElem, label: outgoing[i].label});

            var neighbourId = outgoing[i].trgElem;
            if (!processed.includes(neighbourId)) {
              checks.push({'level': level + 1, 'id': neighbourId});
            }
          }

          processed.push(elem.id);
        }
      }

      // add placeholder-box for missing premises
      if (level == 0) {
        var missing_premise_id = 'MPID';
        var m_premise = '';

        if (elem.label == 'claim') {
          if (language == 'English') {
            m_premise = 'Missing Premise';
          } 
        } else if (elem.label == 'majorClaim') {
          if (language == 'English') {
            m_premise = 'Missing Claim';
          } 
        }
        if (incoming.length <= 0) {
          nodes.push({id: missing_premise_id, label: '\u26A0 ' + m_premise, level: level + 1, font: {color: 'red', size: 20}, color: {border: 'red', background: 'white'}});
          edges.push({from: missing_premise_id, to: elem.id, label: '\u26A1', dashes: true, font: {color: 'red', size: 35}, color: {color: 'red'}});
        }
      }
    }

    return {nodes: new vis.DataSet(nodes), edges: new vis.DataSet(edges)}
}

function getStructureGraph() {
    var nodes = [];
    var edges = [];

    if (analysisResult) {
      for (var i = 0; i < analysisResult.elements.length; ++i) {
        var elem = analysisResult.elements[i];
        var borderColor = 'black';
        var backgroundColor = 'lightblue';
        if (elem.label == 'premise') {
          backgroundColor = 'gold';
        } else if (elem.label == 'majorClaim') {
          backgroundColor = 'orange';
        }

        nodes.push({id: elem.id, label: '', color: {border: borderColor, background: backgroundColor}});
      }

      for (var i = 0; i < analysisResult.relations.length; ++i) {
        var rel = analysisResult.relations[i];
        edges.push({from: rel.srcElem, to: rel.trgElem, label: rel.label});
      }
    }

    return {nodes: new vis.DataSet(nodes), edges: new vis.DataSet(edges)}
}

function detailedGraphUpdate() {

    // destroy graph
    if (detailGraph) {
      detailGraph.destroy();
      detailGraph = null;
    }

    // create graph
    if (selectedClaim || selectedClaim == 0) {
      var elemGraph = getElemGraph(selectedClaim);
      var nodes = elemGraph.nodes;
      var edges = elemGraph.edges;

      var data = {
          nodes: nodes,
          edges: edges
      };

      var options = {
          layout: {
              hierarchical: {
                  direction: 'LR',
                  levelSeparation: 500,
                  nodeSpacing: 80,
                  treeSpacing: 80,
              }
          },
          interaction: {dragNodes :false, dragView: true, zoomView: true, selectable: false, selectConnectedEdges: false},
          physics: {
              enabled: false
          },
          nodes: {
            shadow:false,
            shape: 'box',
            shapeProperties: {
              borderRadius: 0
            },
            font: {align: 'center'},
            widthConstraint: {
              minimum: 330,
              maximum: 330,
            },
            heightConstraint: {
              minimum: 50,
            }
          },
          edges: {
            arrows: { to: {enabled:true, scaleFactor:1}},
            smooth: false,
            shadow:false,
            width: 3,
            font: {
              strokeColor: 'white',
              strokeWidth: 10,
              color: 'black'
            }
          }
      };

      var container = document.getElementById('detailgraph')
      detailGraph = new vis.Network(container, data, options);
    }
}

function structureGraphUpdate() {

    // destroy graph
    if (structureGraph) {
      structureGraph.destroy();
      structureGraph = null;
    }

    // create graph
    var elemGraph = getStructureGraph();
    var nodes = elemGraph.nodes;
    var edges = elemGraph.edges;

    var data = {
        nodes: nodes,
        edges: edges
    };

    var options = {
        layout: {},
        interaction: {dragNodes: true, dragView: true, zoomView: true, selectable: true, selectConnectedEdges: false},
        physics: {
            enabled: true
        },
        nodes: {
          shadow:false,
          shape: 'circle',
          font: {align: 'center'},
        },
        edges: {
          arrows: { to: {enabled:true, scaleFactor:0.5}},
          smooth: false,
          shadow:false,
          width: 3,
          font: {
            strokeColor: 'white',
            strokeWidth: 10,
            color: 'black'
          }
        }
    };


    var container = document.getElementById('structuregraph')
    structureGraph = new vis.Network(container, data, options);


    structureGraph.on("click", function (params) {
      var nodeID = params.nodes[0];
        selectedClaim = null;
      if (nodeID || nodeID == 0) {
        var elem = argElementById(nodeID);
        if (elem && elem.label == 'claim' || elem.label == 'majorClaim') {
          selectedClaim = elem.id;
        }
      }
      detailedGraphUpdate();
    });

}