function noArgumentationAlert() {
    // Unsere Algorithmus hat bisher noch keine argumentativen Strukturen in deinem Text gefunden.\nVersuche doch noch ein paar mehr Sätze zu schreiben und dabei klarer auf die Formulierung der Aussagen (Claims) und Belege (Premissen) zu achten
    alert('Our algorithm has not yet found any argumentative structures in your text.\nTry to write a few more sentences and pay more attention to the wording of the statements (claims) and evidence (premises).');
}

function noChatGPTResponseAlert() {
    alert('Seems like chat-GPT could not return an answer at the moment. Please try again.')
}

function argElementById(id) {
    if (analysisResult) {
        for (var i = 0; i < analysisResult.elements.length; ++i) {
        var elem = analysisResult.elements[i];
        if (elem.id == id) {
            return elem;
        }
        }
    }
    return null;
}

function argElementsByLabel(label) {
    var res = [];
    if (analysisResult) {
      for (var i = 0; i < analysisResult.elements.length; ++i) {
        var elem = analysisResult.elements[i];
        if (elem.label == label) {
          res.push(elem);
        }
      }
    }
    return res;
}

function getRelations(elemId, incoming, outgoing) {
    var res = [];
    if (analysisResult) {
      for (var i = 0; i < analysisResult.relations.length; ++i) {
        var relation = analysisResult.relations[i];
        if (outgoing && relation.srcElem == elemId) {
          res.push(relation);
        }
        if (incoming && relation.trgElem == elemId) {
          res.push(relation);
        }
      }
    }
    return res;
}

function mappingInsert(mapping, idx, length) {
    mapping.push([idx, length]);
}

function mappingIdx(mapping, idx) {
    delta = 0;
    for (var i = 0; i < mapping.length; i++) {
      if (idx >= mapping[i][0]) {
        delta = delta + mapping[i][1];
      }
    }
    return idx + delta;
}