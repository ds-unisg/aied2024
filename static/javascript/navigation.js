// Tabs switching

function openTab(evt, tabName) {
    var i, tabContent, tabButtons;
    
    tabContent = document.getElementsByClassName("tab-content");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].classList.remove("active");
    }

    tabButtons = document.getElementsByClassName("tab-button");
    for (i = 0; i < tabButtons.length; i++) {
        tabButtons[i].classList.remove("active");
        document.getElementById("button_"+tabName).classList.add("active");
    }

    document.getElementById(tabName).classList.add("active");
    evt.currentTarget.classList.add("active");
}

// Open the first tab by default
document.getElementById("tab_init").classList.add("active");
// document.getElementsByClassName("tab-button")[0].classList.add("active");