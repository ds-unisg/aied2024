function onEditorClick() {
    if (!justSelectedClaim) {
      selectedClaim = null;
      detailedGraphUpdate();
    }
    justSelectedClaim = false
}


function updateWordCount() {
    var editor = document.getElementById("editor");
    var value = editor.innerText;
    var wordCount = 0;
    if (value.length > 0) {
      wordCount = value.split(' ').length;
    }
    var d = document.getElementById("wordcount");
    d.innerHTML = wordCount;
}

function onEditorClaimClick(elem) {
    selectedClaim = elem.getAttribute('data-id');
    justSelectedClaim = true
    detailedGraphUpdate();
}

function onBodyResize() {
    if (detailGraph) {
      detailGraph.redraw();
      //detailedGraphUpdate(); // this may be required to avoid bugs
    }
    if (structureGraph) {
      structureGraph.redraw();
      //structureGraphUpdate(); // this may be required to avoid bugs
    }
}