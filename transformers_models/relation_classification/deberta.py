from sentence_transformers import CrossEncoder

model = CrossEncoder("cross-encoder/nli-deberta-v3-large")

label_mapping = ["contradicts", "entails", "neutral"]


def deberta_predict_relations(premise: str, hypothesis: str):
    scores = model.predict([[premise, hypothesis]])
    label = label_mapping[scores.argmax(axis=1)[0]]

    return label
