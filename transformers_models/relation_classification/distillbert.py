from sentence_transformers import CrossEncoder

model = CrossEncoder("textattack/distilbert-base-uncased-MNLI")

label_mapping = ["contradicts", "entails", "neutral"]


def distillbert_predict_relations(premise: str, hypothesis: str):
    scores = model.predict([(premise, hypothesis)])
    label = label_mapping[scores.argmax(axis=1)[0]]

    return label
