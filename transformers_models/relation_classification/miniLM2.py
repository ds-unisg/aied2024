from sentence_transformers import CrossEncoder

model = CrossEncoder("cross-encoder/nli-MiniLM2-L6-H768")

label_mapping = ["contradicts", "entails", "neutral"]


def minilm2_predict_relations(premise: str, hypothesis: str):
    scores = model.predict([(premise, hypothesis)])
    label = label_mapping[scores.argmax(axis=1)[0]]

    return label
