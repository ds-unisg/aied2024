import torch
from transformers import AutoModelForSequenceClassification, AutoTokenizer

model = AutoModelForSequenceClassification.from_pretrained(
    "ynie/xlnet-large-cased-snli_mnli_fever_anli_R1_R2_R3-nli"
)
tokenizer = AutoTokenizer.from_pretrained(
    "ynie/xlnet-large-cased-snli_mnli_fever_anli_R1_R2_R3-nli"
)

label_mapping = ["contradicts", "entails", "neutral"]


def xlnet_predict_relations(premise: str, hypothesis: str):
    features = tokenizer([premise], [hypothesis], padding=True, return_tensors="pt")
    model.eval()

    with torch.no_grad():
        scores = model(**features).logits
    # print(f"Premise: {premise}\nHypothesis: {hypothesis}\nLabel: {label_mapping[scores.argmax()]}\n##############################################################################   ")
    return label_mapping[scores.argmax()]
