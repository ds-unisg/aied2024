from pathlib import Path

import spacy
import torch
from torch import nn
from transformers import BertModel, BertTokenizer

nlp = spacy.load("en_core_web_sm")

model_cache = {}

labels_mapping = {0: "MajorClaim", 1: "Claim", 2: "Premise", 3: "None"}


def load_model_tokenizer():
    if "model_and_tokenizer" not in model_cache:
        tokenizer = BertTokenizer.from_pretrained("bert-base-cased")
        model = BertClassifier()
        model.load_state_dict(
            torch.load(
                Path(__file__).parent.joinpath(
                    "fine_tuned_bert_on_FP",
                    "classifier_feedback_prize(map_1_lead_as_None).pt",
                ),
                map_location=torch.device("cpu"),
            )
        )
        model = torch.compile(model)
        model.eval()
        model_cache["model_and_tokenizer"] = model, tokenizer

    return model_cache["model_and_tokenizer"]


def predict_bert(input_essay):
    predictions = []
    input_essay = input_essay.replace("\n\n", " ")
    input_essay = input_essay.replace("\n", " ")
    input_essay = input_essay.replace("  ", " ")

    input_doc = nlp(input_essay)
    input_essay_sentences = [sen.text for sen in input_doc.sents]

    model, tokenizer = load_model_tokenizer()

    for sentence in input_essay_sentences:
        test_input = tokenizer(
            sentence,
            padding="max_length",
            max_length=512,
            truncation=True,
            return_tensors="pt",
        )

        mask = test_input["attention_mask"]
        input_id = test_input["input_ids"].squeeze(1)

        with torch.no_grad():
            output = model(input_id, mask)
        # print(output)

        res = output.argmax(dim=1)

        predictions.append(labels_mapping[res.item()])

    return predictions


class BertClassifier(nn.Module):
    def __init__(self, dropout=0.5):
        super(BertClassifier, self).__init__()

        self.bert = BertModel.from_pretrained("bert-base-cased")
        self.dropout = nn.Dropout(dropout)
        self.linear = nn.Linear(768, 7)
        self.relu = nn.ReLU()

    def forward(self, input_id, mask):
        _, pooled_output = self.bert(
            input_ids=input_id, attention_mask=mask, return_dict=False
        )
        dropout_output = self.dropout(pooled_output)
        linear_output = self.linear(dropout_output)
        final_layer = self.relu(linear_output)

        return final_layer
