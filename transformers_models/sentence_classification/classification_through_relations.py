import spacy

from transformers_models.relation_classification.bart import bart_predict_relations
from transformers_models.relation_classification.deberta import (
    deberta_predict_relations,
)
from transformers_models.relation_classification.distillbert import (
    distillbert_predict_relations,
)
from transformers_models.relation_classification.miniLM2 import (
    minilm2_predict_relations,
)
from transformers_models.relation_classification.xlnet import xlnet_predict_relations

nlp = spacy.load("en_core_web_sm")


def predict_through_relations(input_text, rel_cls):
    predictions = []

    input_doc = nlp(input_text)
    input_text_sentences = [sen.text for sen in input_doc.sents]
    output_relations = {}

    # Initiating a dictionary for all sentences - keeps counts
    for sentence in input_text_sentences:
        output_relations[sentence] = {"supported_by": 0, "supports": 0}

    for i in range(len(input_text_sentences)):
        premise = input_text_sentences[i]

        for j in range(i + 1, len(input_text_sentences)):
            hypothesis = input_text_sentences[j]

            if rel_cls == "deberta":
                relation = deberta_predict_relations(premise, hypothesis)
            elif rel_cls == "miniLM2":
                relation = minilm2_predict_relations(premise, hypothesis)

            if relation != "neutral" and relation != "contradicts":
                output_relations[premise]["supports"] += 1
                output_relations[hypothesis]["supported_by"] += 1

    for key in output_relations.keys():
        if (
            output_relations[key]["supported_by"] == 0
            and output_relations[key]["supports"] == 0
        ):
            predictions.append("None")

        elif (
            output_relations[key]["supported_by"] != 0
            and output_relations[key]["supports"] == 0
        ):
            predictions.append("Claim")

        elif (
            output_relations[key]["supported_by"] == 0
            and output_relations[key]["supports"] != 0
        ):
            predictions.append("Premise")

        elif (
            output_relations[key]["supported_by"] != 0
            and output_relations[key]["supports"] != 0
        ):
            if (
                output_relations[key]["supported_by"]
                == output_relations[key]["supports"]
            ):
                predictions.append("Claim")

            elif (
                output_relations[key]["supported_by"]
                > output_relations[key]["supports"]
            ):
                predictions.append("Claim")

            elif (
                output_relations[key]["supported_by"]
                < output_relations[key]["supports"]
            ):
                predictions.append("Premise")

            else:
                predictions.append("Claim")

    return predictions
